[masters]
master ansible_host=165.227.178.29 ansible_user=root

[workers]
worker1 ansible_host=165.227.189.0 ansible_user=root
worker2 ansible_host=159.203.84.132 ansible_user=root

[all:vars]
ansible_python_interpreter=/usr/bin/python3
